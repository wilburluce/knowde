import {Component} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export interface Product {
  name: string;
  price: number;
}
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  productList$= new BehaviorSubject<Product[]>([])
  printerOut$ = new BehaviorSubject<string>('')
  constructor() {}

}
