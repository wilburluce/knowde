import {Component, Input, OnInit} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {Product} from "../nav/nav.component";

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.scss']
})
export class OutputComponent implements OnInit {
  @Input() productList$: BehaviorSubject<Product[]>

  constructor() {
  }

  ngOnInit() {
  }

  printLast() {
    let last: Product;
    try {
      last = this.productList$.value.slice(-1)[0]
    } catch (e) {
    }
    if (last) {
      return `${last.name} ${last.price || 'not found'}`
    }
  }

}
