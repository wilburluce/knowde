import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from "@angular/forms";
import {BehaviorSubject} from "rxjs";
import {Product} from "../nav/nav.component";

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnInit {
  products = {'prod1': 12, 'prod2': 14, 'prod3': 16}
  scannedProducts: Product[] = []
  myControl = new FormControl();
  @Input() productList$: BehaviorSubject<Product[]>
  @Input() printerOut$: BehaviorSubject<string>

  constructor() {
  }

  ngOnInit() {
  }

  findProd() {
    const result = this.products[this.myControl.value] || 0
    const newVal = {name: this.myControl.value, price: result}
    if (result) {
      this.scannedProducts.push(newVal)
    }
    this.productList$.next([newVal])
  }

  sumProducts() {
    const sum = this.scannedProducts.reduce((sum, curr) => sum + curr.price, 0)
    return sum
  }

  handleExit() {
    this.printerOut$.next(this.sumProducts().toString())
    this.productList$.next(this.scannedProducts)
    console.log('sum', this.sumProducts())
  }

}
