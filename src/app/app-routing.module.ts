import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ScannerComponent} from "./components/scanner/scanner.component";
import {OutputComponent} from "./components/output/output.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'scanner',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false, enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
